import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useloginStore = defineStore("login", () => {
  const LoginName = ref("");
  const isLogin = computed(() => {
    return LoginName.value !== "";
  });
  const login = (userName: string): void => {
    LoginName.value = userName;
    localStorage.setItem("LoginName", userName);
  };
  const Logout = (): void => {
    LoginName.value = "";
    localStorage.removeItem("LoginName");
  };
  const loadData = (): void => {
    LoginName.value = localStorage.getItem("LoginName") || "";
  };

  return { LoginName, isLogin, login, Logout, loadData };
});
